/**
 * @finalidad 
 * @author Yamel Senih
 * @date 01/04/2012
 */
package com.cds.getweight.util;

import java.sql.Timestamp;

/**
 * @author Yamel Senih
 *
 */
public interface I_PrinterFiscalHasar {

	public void printCommand(String cmd) throws Exception;
	public void printCommand(String [] cmd) throws Exception;
	public void printHeader(String nameBP, String rfc, String nroVoucher, 
			String codPrinter, String dateVoucher, String timeVoucher, String docType) throws Exception;
	public void printLine(String product, double price, double units, 
			double taxRate, String facProd, String codProduct) throws Exception;
	public void printTextHeader(String sheader) throws Exception;
    public void printMessage(String smessage) throws Exception;
    public void printTextTrailer(String strailer) throws Exception;
    public void printTotal(String sPayment, double dpaid) throws Exception;
    //	Report Y - Z
    public void printZReport() throws Exception;
    public void printXReport() throws Exception;
    //	Report Memory Info
    public void printFiscalMemReport(Timestamp dateFrom, Timestamp dateTo, String type) throws Exception;
    public void printFiscalMemReport(int zFrom, int zTo, String type) throws Exception;
}
