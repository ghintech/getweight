/**
 * @finalidad 
 * @author Yamel Senih
 * @date 16/12/2010
 */
package com.cds.getweight.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;
import java.util.Vector;

/*import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;*/
import com.fazecast.jSerialComm.*;
//import org.compiere.util.CLogger;

/**
 * @author Yamel Senih
 *
 */
public class ManejoPuerto {
	
	/**
	 * 
	 * *** Constructor de la Clase ***
	 * @author Yamel Senih 01/02/2011, 10:43:33
	 */
	public ManejoPuerto(String nombPuerto, 
			int baudios, 
			int bitDatos, 
			int bitParada, 
			int paridad) {
		this.nombPuerto = nombPuerto;
		this.baudios = baudios;
		this.bitDatos = bitDatos;
		this.bitParada = bitParada;
		this.paridad = paridad;
	}
	
	/**
	 * Obtiene la lista de puertos del equipo
	 * @author Yamel Senih 16/12/2010, 10:29:25
	 * @return
	 * @return Enumeration<?>
	 */
	/*public Enumeration<?> getListaPuertos(){
		listaPuerto = CommPortIdentifier.getPortIdentifiers();
		return listaPuerto;
	}*/
	
	/**
	 * Obtiene Los nombre de los puertos COM existentes en el equipo
	 * @author Yamel Senih 01/02/2011, 10:49:58
	 * @return
	 * @return Vector<Object>
	 */
	/*public Vector<Object> getNombrePuertosCOM(){
		CommPortIdentifier puerto;
		Vector<Object> vecNombPuerto = new Vector<Object>();
		Enumeration<?> lisPuerto = CommPortIdentifier.getPortIdentifiers();
		while (lisPuerto.hasMoreElements()) {
			puerto = (CommPortIdentifier) lisPuerto.nextElement();
			if(puerto.getPortType() == CommPortIdentifier.PORT_SERIAL){
				vecNombPuerto.addElement(puerto.getName());
			}
		}
		return vecNombPuerto;
	}*/
	
	/**
	 * Obtiene el canal de entrada del puerto
	 * @author Yamel Senih 16/12/2010, 10:57:27
	 * @return
	 * @return InputStream
	 */
	public InputStream getEntrada() {
		return entrada;
	}
	
	/**
	 * Obtiene el canal de salida del puerto
	 * @author Yamel Senih 16/12/2010, 10:58:13
	 * @return
	 * @return OutputStream
	 */
	public OutputStream getSalida() {
		return salida;
	}
	
	/**
	 * Agrega un manejador de eventos al puerto
	 * @author Yamel Senih 04/02/2011, 12:16:18
	 * @param evtPuerto
	 * @throws TooManyListenersException
	 * @return void
	 */
	/*public void agregarEscuchaPuerto(EventosPuerto evtPuerto) throws TooManyListenersException{
		puertoSerie.addEventListener(evtPuerto);
	}*/
	
	/**
	 * Si se notifica o no cuando haya datos disponibles
	 * @author Yamel Senih 04/02/2011, 12:17:46
	 * @param resp
	 * @return void
	 */
	/*public void notDatosDisponible(boolean resp){
		puertoSerie.notifyOnDataAvailable(resp);
	}*/
	
	/**
	 * Obtiene el puerto Serie
	 * @author Yamel Senih 04/02/2011, 12:19:06
	 * @return
	 * @return SerialPort
	 */
	public SerialPort getPuertoSerie(){
		return puertoSerie;
	}
	
	/**
	 * Abre el puerto serie
	 * @author Yamel Senih 16/12/2010, 11:47:03
	 * @return
	 * @return SerialPort
	 * @throws PortInUseException 
	 * @throws UnsupportedCommOperationException
	 * @throws Exception
	 */
	/*public SerialPort abrirPuerto() throws PortInUseException, UnsupportedCommOperationException, Exception {
		listaPuerto = CommPortIdentifier.getPortIdentifiers();
		System.out.println(listaPuerto.hasMoreElements());
		while (listaPuerto.hasMoreElements()) {
			idPuerto = (CommPortIdentifier) listaPuerto.nextElement();
			System.out.println("Port " + idPuerto.getName());
			if(idPuerto.getPortType() == CommPortIdentifier.PORT_SERIAL){
				if(idPuerto.getName().equals(nombPuerto)){
					System.out.println("Located " + nombPuerto);
					System.out.println("Opening " + nombPuerto);
					puertoSerie = (SerialPort) idPuerto.open("Server RS-232...", 2000);
					System.out.println("Parameterizing Port...");
					puertoSerie.setSerialPortParams( baudios,
							bitDatos,
							bitParada,
							paridad);
					System.out.println("Port Ready ...");
					entrada = puertoSerie.getInputStream();
					salida = puertoSerie.getOutputStream();
					return puertoSerie;
				}
			}
		}
		throw new Exception("Port " + nombPuerto + " Not Found "); 
	}*/
	/**
	 * Abre el puerto serie
	 */
	public SerialPort abrirPuerto2(int longitudTrama) throws  Exception {
		SerialPort comPort = SerialPort.getCommPort(nombPuerto);
		comPort.openPort();
		int numRead=0;
		try {
		   while (numRead!=longitudTrama)
		   {
		      while (comPort.bytesAvailable() == 0)
		         Thread.sleep(20);

		      byte[] readBuffer = new byte[comPort.bytesAvailable()];
		      numRead = comPort.readBytes(readBuffer, readBuffer.length);
		      String text = new String(readBuffer);
		      System.out.println("Read " + numRead + " bytes.");
		      System.out.println("Text " + text);
		      entradaText=text;
		      
		      
		   }
		} catch (Exception e) { e.printStackTrace(); }
		comPort.closePort();
		return comPort;
		//
	}
	/**
	 * Cierra el puerto serie
	 * @author Yamel Senih 16/12/2010, 11:48:30
	 * @return void
	 * @throws IOException 
	 */
	/*public void cerrarPuerto() throws IOException{
		if(puertoSerie != null){
			entrada.close();
			salida.close();
			puertoSerie.close();
			System.out.println("Closed " + nombPuerto);
		}
	}*/

	/**	Logger						*/
	//private static CLogger log = CLogger.getCLogger(ManejoPuerto.class);
	
	//private static CommPortIdentifier idPuerto;
	private static Enumeration<?> listaPuerto;
	public String getEntradaText() {
		return entradaText;
	}

	public void setEntradaText(String entradaText) {
		this.entradaText = entradaText;
	}

	private SerialPort puertoSerie;
	private InputStream entrada;
	private String entradaText;
	private OutputStream salida;
	private String nombPuerto;
	private int baudios;
	private int bitDatos;
	private int bitParada;
	private int paridad;
}
