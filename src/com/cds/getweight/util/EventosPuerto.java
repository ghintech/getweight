/**
 * 
 */
package com.cds.getweight.util;

import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;

public abstract class EventosPuerto implements SerialPortEventListener{

	private static final long serialVersionUID = -1277688319324654146L;

	@Override
	public void serialEvent(SerialPortEvent ev) {
		switch( ev.getEventType() ) {
			case SerialPortEvent.BI:
			case SerialPortEvent.OE:
			case SerialPortEvent.FE:
			case SerialPortEvent.PE:
			case SerialPortEvent.CD:
			case SerialPortEvent.CTS:
			case SerialPortEvent.DSR:
			case SerialPortEvent.RI:
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
				break;
			case SerialPortEvent.DATA_AVAILABLE:
				datosDisp();				
				break;
		}
	}

	public abstract void datosDisp();
}
