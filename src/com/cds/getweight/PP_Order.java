package com.cds.getweight;

import java.math.BigDecimal;

public class PP_Order {

	private BigDecimal Weight;
	private BigDecimal Weight2;
	private int PP_Order_ID;
	private int AD_Org_ID;
	public BigDecimal getWeight() {
		return Weight;
	}
	public void setWeight(BigDecimal weight) {
		Weight = weight;
	}
	public BigDecimal getWeight2() {
		return Weight2;
	}
	public void setWeight2(BigDecimal weight2) {
		Weight2 = weight2;
	}
	public int getPP_Order_ID() {
		return PP_Order_ID;
	}
	public void setPP_Order_ID(int pP_Order_ID) {
		PP_Order_ID = pP_Order_ID;
	}
	public int getAD_Org_ID() {
		return AD_Org_ID;
	}
	public void setAD_Org_ID(int aD_Org_ID) {
		AD_Org_ID = aD_Org_ID;
	}
	
	
}
