package com.cds.getweight;

import org.idempiere.webservice.client.base.LoginRequest;
import org.idempiere.webservice.client.exceptions.WebServiceException;
import org.idempiere.webservice.client.net.WebServiceConnection;
import org.idempiere.webservice.client.request.QueryDataRequest;
import org.idempiere.webservice.client.request.UpdateDataRequest;
import org.idempiere.webservice.client.response.StandardResponse;
import org.idempiere.webservice.client.response.WindowTabDataResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cds.getweight.util.EventosPuerto;
import com.cds.getweight.util.ManejoPuerto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.idempiere.webservice.client.base.DataRow;
import org.idempiere.webservice.client.base.Enums;
import org.idempiere.webservice.client.base.Enums.WebServiceResponseStatus;
import org.idempiere.webservice.client.base.Field;

public class getweight {
	
	private static String wsWeightType = "WeightOrder";
	private static String wsUpdateWeightType = "WeightUpdate";
	
	
	private static int AD_Client_ID;
	private static int AD_Org_ID;
	private static int AD_Role_ID;
	private static int M_Warehouse_ID;
	private static String UrlBase = "http://localhost:8080";
	private static String UrlBase2 = "erp.casadelsoftware.com";
	private static String UserName;
	private static String UserPass;
	private static String Language;

	private static QueryDataRequest ws;
	private static UpdateDataRequest ws2;
	private static int OffSet = 1;
	private static int Limit = 1000;
	
	private static String configFile="getweight.properties";
	//private static int zlength;
	//private static int znumber;
	//private static String serialnumber;
	private static int C_FiscalPrintConf_ID;
	

//	Entrada Romana
	private static String trama="= 170000";
	private static String puerto="COM1";
	private static int baudios=9600;
	private static int bitDatos=8;
	private static int bitParada=1;
	private static int paridad=0;
	private static int ascInicio;
	private static int ascFin;
	private static int posInicioCorte;
	private static int posFinCorte;
	private static int vPosInicioCorte;
	private static int vPosFinCorte;
	private static int longitudTrama;
	private static boolean leer;
	private static boolean encendido;
	private static EventosPuerto evtPuerto;
	private static ManejoPuerto mPuerto;
	private static InputStream canalEnt;
	//private static BigDecimal weight;

	public static String procesarTrama() {
		int qtybefore=0;
		while(trama.substring(posInicioCorte-qtybefore-1, posInicioCorte).compareTo(" ")!=0) {
			qtybefore++;
		}
		if(trama.length() == longitudTrama){
			String test=trama.substring(posInicioCorte-qtybefore, posFinCorte-qtybefore).trim();
			return trama.substring(posInicioCorte-qtybefore, posFinCorte-qtybefore).trim();
		}else{
			System.out.println("Incompleta");
			return "0";
		}
	}

	

	public static void main(String[] arg) throws Exception {

		//Inicio del Servicio de Recepcion de RS-232
				
				
		System.out.println("************************************************************");
		System.out.println("****               PESAJE DE MATERIAL                  *****");
		System.out.println("************************************************************");
		if(arg.length>0){
			int count =1;
			for (String s: arg) {

				if(count == 1)
					configFile= s.trim();
				count++;

			}
		}
		System.out.println("Leyendo Archivo de configuracion:");
		System.out.println(configFile);
		
		try {
			File fXmlFile = new File(configFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("entry");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					switch (eElement.getAttribute("key")) {
					
					case "AD_Client_ID":
						AD_Client_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Client_ID);
						break;
					case "AD_Org_ID":
						AD_Org_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Org_ID);
						break;
					
					case "AD_Role_ID":
						AD_Role_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(AD_Role_ID);
						break;
					case "M_Warehouse_ID":
						M_Warehouse_ID = Integer.valueOf(eElement.getTextContent());
						//System.out.println(M_Warehouse_ID);
						break;

					case "UrlBase":
						UrlBase = eElement.getTextContent();
						//System.out.println(UrlBase);
						break;

					case "UrlBase2":
						UrlBase2 = eElement.getTextContent();
						//System.out.println(UrlBase2);
						break;
					case "UserName":
						UserName = eElement.getTextContent();
						//System.out.println(UserName);
						break;
					case "UserPass":
						UserPass = eElement.getTextContent();
						//System.out.println(UserPass);
						break;
					case "Language":
						Language = eElement.getTextContent();
						//System.out.println(Language);
						break;
					case "puerto":
						puerto = eElement.getTextContent();
						//System.out.println(PrinterPort);
						break;
					case "wsWeightType":
						wsWeightType = eElement.getTextContent();
						//System.out.println(wsInvoiceType);
						break;
					case "ascInicio":
						ascInicio = Integer.valueOf(eElement.getTextContent());
						//System.out.println(UpdateDocumentNo);
						break;
					case "ascFin":
						ascFin = Integer.valueOf(eElement.getTextContent());
						//System.out.println(Country);
						break;
					case "posInicioCorte":
						vPosInicioCorte = Integer.valueOf(eElement.getTextContent());
						posInicioCorte = Integer.valueOf(eElement.getTextContent());
						//System.out.println(Vendor);
						break;
					case "posFinCorte":
						vPosFinCorte = Integer.valueOf(eElement.getTextContent());
						posFinCorte = Integer.valueOf(eElement.getTextContent());
						//System.out.println(SpoolerFilePath);
					case "longitudTrama":
						longitudTrama = Integer.valueOf(eElement.getTextContent());
						//System.out.println(Model);
						break;
					
					default:
						break;

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		createWS();
		System.out.println("Esperando solicitud...");
		while(true) {

			if(!pp_order()) {
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static boolean pp_order() throws Exception {

		WebServiceConnection client = getClient();
		PP_Order[] pp_order=null;
		try {

			do{

				WindowTabDataResponse response = client.sendRequest(ws);
				if (response.getStatus() == WebServiceResponseStatus.Error) {
					System.out.println(response.getErrorMessage());
					return false;
				} else {


					if(response.getNumRows()>0) {
						System.out.println("Total rows: " + response.getTotalRows());
						System.out.println("Num rows: " + response.getNumRows());
						System.out.println("Start row: " + response.getStartRow());
						OffSet+=Limit;
						if(response.getTotalRows()<Limit) {
							OffSet=0;
						}

					} else {
						OffSet=0;
						return false;
					}
					pp_order = new PP_Order[response.getNumRows()];
					for (int i = 0; i < response.getDataSet().getRowsCount(); i++) {
						//System.out.println("Row: " + (i + 1)+" of "+response.getDataSet().getRowsCount());
						pp_order[i] = new PP_Order();


						for (int j = 0; j < response.getDataSet().getRow(i).getFieldsCount(); j++) {
							Field field = response.getDataSet().getRow(i).getFields().get(j);
							//System.out.print(" " + field.getColumn() + " = " + field.getValue() + "  ");
							String fieldValue = String.valueOf(field.getValue());

							switch (field.getColumn()) {
							
							case "PP_Order_ID":
								pp_order[i].setPP_Order_ID(Integer.valueOf(fieldValue));
								break;
						
							case "Weight":
								pp_order[i].setWeight(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							case "Weight2":
								pp_order[i].setWeight2(BigDecimal.valueOf(Double.valueOf(fieldValue)));
								break;
							
							case "AD_Org_ID":
								pp_order[i].setAD_Org_ID(Integer.valueOf(fieldValue));
								break;

							
							default:
								break;
							}
						}
						lecturaPuerto(pp_order[i]);
					}
					
				}
				
				ws.setOffset(OffSet);
			}while (OffSet!=0);
			

			
		} catch (WebServiceException | NumberFormatException e) {
			System.out.println("Ha ocurrido un error leyendo valores ");
		}
		return true;
	}
	public static void lecturaPuerto(PP_Order pp_order) throws Exception{
		mPuerto = new ManejoPuerto(puerto, 
				baudios, 
				bitDatos, 
				bitParada, 
				paridad);
		mPuerto.abrirPuerto2(longitudTrama);
		canalEnt = mPuerto.getEntrada();
		//mPuerto.agregarEscuchaPuerto(evtPuerto);
		//mPuerto.notDatosDisponible(true);
		encendido = true;
		/*try {
			
			while(canalEnt.available() > 0) {
				int bit = canalEnt.read();
				if(bit == ascInicio && leer == false){
					leer = true;
					trama = "";
				}
				if(leer)
					trama += (char)bit;
				if(leer && bit == ascFin && trama.length() == longitudTrama){
					leer = false;*/
					trama=mPuerto.getEntradaText();//.substring(vPosInicioCorte, vPosFinCorte);
					if(trama!=null)
						SendWeight(pp_order);
	/*			} 
			}
        } catch( Exception e ) {
        	System.out.println(e.getLocalizedMessage());
        	
        }*/
	}	//	lecturaPuerto	
	
	


	private static void createWS() {

		ws = new QueryDataRequest();
		ws.setWebServiceType(wsWeightType);
		ws.setLogin(getLogin());
		ws.setLimit(Limit);


		//ws.setFilter("(AD_Org_ID=0 OR AD_Org_ID="+getAD_Org_ID()
		//+") AND WasPrinted='N' AND Script IS NOT NULL"
		//+" AND C_FiscalPrintConf_ID = "+getC_FiscalPrintConf_ID());
		DataRow data = new DataRow();
		data.addField("AD_Org_ID",getAD_Org_ID());
		data.addField("ReferenceNo","IP");
	
		ws.setDataRow(data);

	}
	private static void updateWS(int PP_Order_ID) {

		ws2 = new UpdateDataRequest();
		ws2.setWebServiceType("WeightUpdate");
		ws2.setRecordID(PP_Order_ID);
		ws2.setLogin(getLogin());

	}
	public static LoginRequest getLogin() {
		LoginRequest login = new LoginRequest();
		login.setUser(getUserName());
		login.setPass(getUserPass());
		login.setLang(Enums.Language.valueOf(getLanguage()));
		login.setClientID(getAD_Client_ID());
		login.setRoleID(getAD_Role_ID());
		login.setOrgID(getAD_Org_ID());
		login.setWarehouseID(getM_Warehouse_ID());
		return login;
	}
	public static WebServiceConnection getClient() {
		WebServiceConnection client = new WebServiceConnection();
		client.setAttempts(3);
		client.setTimeout(200000);
		client.setAttemptsTimeout(200000);
		client.setUrl(getUrlBase());
		client.setAppName("PRIM Pesada de material");
		return client;
	}
	public static boolean SendWeight(PP_Order pp_order) {
		
		

		updateWS(pp_order.getPP_Order_ID());
		try {
			//zlength = 1;
			//znumber = 0;
			//serialnumber = "";
			//invoice.setWasPrinted("Y");
			
			DataRow data =new DataRow();

			
			double valortrama=Double.valueOf(procesarTrama());
			//data.addField("WasPrinted",invoice.getWasPrinted());
			if(pp_order.getWeight2().compareTo(BigDecimal.ZERO)==0){
				System.out.println("Obteniendo PESO BRUTO");
					data.addField("Weight2",BigDecimal.valueOf(valortrama));
					data.addField("Weight",BigDecimal.ZERO);
					data.addField("Weight3",BigDecimal.ZERO);
					data.addField("ReferenceNo","");
					data.addField("DateFrom",new Timestamp(System.currentTimeMillis()));
			}else if(pp_order.getWeight2().compareTo(BigDecimal.ZERO)!=0 && pp_order.getWeight().compareTo(BigDecimal.ZERO)==0){
				System.out.println("Obteniendo PESO TARA");
				data.addField("Weight",BigDecimal.valueOf(valortrama));
				
				data.addField("Weight3",pp_order.getWeight2().subtract(BigDecimal.valueOf(valortrama)));
				data.addField("ReferenceNo","");
				data.addField("DateTo",new Timestamp(System.currentTimeMillis()));
			}

			

			ws2.setDataRow(data);
			WebServiceConnection client = getClient();

			try {
				StandardResponse response;
				response = client.sendRequest(ws2);
				System.out.println(response.getErrorMessage());
				if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {

					return false;
				} else{
					return true;

				}
			} catch (WebServiceException | NumberFormatException e) {
			}

		}
		catch (Exception e) {
			System.out.println("No se pudo enviar el archivo de estatus se intentara con el servidor alternativo");
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	
	


	public static int getAD_Client_ID() {
		return AD_Client_ID;
	}
	public static void setAD_Client_ID(int aD_Client_ID) {
		AD_Client_ID = aD_Client_ID;
	}
	public static int getAD_Org_ID() {
		return AD_Org_ID;
	}
	public static void setAD_Org_ID(int aD_Org_ID) {
		AD_Org_ID = aD_Org_ID;
	}
	public static int getAD_Role_ID() {
		return AD_Role_ID;
	}
	public static void setAD_Role_ID(int aD_Role_ID) {
		AD_Role_ID = aD_Role_ID;
	}
	public static int getM_Warehouse_ID() {
		return M_Warehouse_ID;
	}
	public static void setM_Warehouse_ID(int m_Warehouse_ID) {
		M_Warehouse_ID = m_Warehouse_ID;
	}
	public static String getUserName() {
		return UserName;
	}
	public static void setUserName(String userName) {
		UserName = userName;
	}
	public static String getUserPass() {
		return UserPass;
	}
	public static void setUserPass(String userPass) {
		UserPass = userPass;
	}
	public static String getLanguage() {
		return Language;
	}
	public static void setLanguage(String language) {
		Language = language;
	}
	public static String getUrlBase() {
		return UrlBase;
	}
	public static void setUrlBase(String urlBase) {
		UrlBase = urlBase;
	}
	public static String getUrlBase2() {
		return UrlBase2;
	}
	public static void setUrlBase2(String urlBase2) {
		UrlBase2 = urlBase2;
	}

	public static String getWsInvoiceType() {
		return wsWeightType;
	}
	public static void setWsInvoiceType(String wsInvoiceType) {
		getweight.wsWeightType = wsInvoiceType;
	}

	public static String getWsUpdateInvoiceType() {
		return wsUpdateWeightType;
	}
	public static void setWsUpdateInvoiceType(String wsUpdateInvoiceType) {
		getweight.wsUpdateWeightType = wsUpdateInvoiceType;
	}
	public static int getC_FiscalPrintConf_ID() {
		return C_FiscalPrintConf_ID;
	}
	/*public void detenerServicio(){
		if(encendido){
			try {
				mPuerto.cerrarPuerto();
				encendido = false;
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}	//detenerServicio
*/
}
