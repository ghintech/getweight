package com.cds.getweight;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class C_Invoice {
	private int C_Invoice_ID;
	private String DocumentNo;
	private String Script;
	private String WasPrinted;
	private String FiscalNumber;
	private int C_Invoice_Fiscal_ID;
	private int line;
	private String productvalue;
	private String productname;
	private BigDecimal qtyentered;
	private BigDecimal priceentered;
	private BigDecimal qtyinvoiced;
	private BigDecimal pricelist;
	private String  linedescription;
	private String ad_language;
	//private String taxindicator;
	//private BigDecimal taxamt;
	private BigDecimal taxrate;
	//private int c_tax_id;
	private String bpvalue;
	private String bpname;
	private Timestamp dateinvoiced;
	private String orderdocumentno;
	private String orderporeference;
	private String bpaddress1;
	private String bpaddress2;
	private String bpaddress3;
	private String bpaddress4;
	private String bplocationname;
	private String bpcity;
	private String bpregion;
	private String paymentterm;
	private String salesrepname;
	private String DocBaseType;
	private String Description;
	private String UPC;
	private String BPTaxID;
	
	
	
	public String getBptaxid() {
		return BPTaxID;
	}
	public void setBptaxid(String bptaxid) {
		this.BPTaxID = bptaxid;
	}
	public int getC_Invoice_ID() {
		return C_Invoice_ID;
	}
	public void setC_Invoice_ID(int c_Invoice_ID) {
		C_Invoice_ID = c_Invoice_ID;
	}
	public String getDocumentNo() {
		return DocumentNo;
	}
	public void setDocumentNo(String documentNo) {
		DocumentNo = documentNo;
	}
	public String getScript() {
		return Script;
	}
	public void setScript(String script) {
		Script = script;
	}
	public String getWasPrinted() {
		return WasPrinted;
	}
	public void setWasPrinted(String wasPrinted) {
		WasPrinted = wasPrinted;
	}
	public String getFiscalNumber() {
		return FiscalNumber;
	}
	public void setFiscalNumber(String fiscalNumber) {
		FiscalNumber = fiscalNumber;
	}
	public void setC_Invoice_Fiscal_ID(int C_Invoice_Fiscal_ID) {
		this.C_Invoice_Fiscal_ID = C_Invoice_Fiscal_ID;
	}
	public int getC_Invoice_Fiscal_ID() {
		return C_Invoice_Fiscal_ID;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public String getProductvalue() {
		return productvalue;
	}
	public void setProductvalue(String productvalue) {
		this.productvalue = productvalue;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public BigDecimal getQtyentered() {
		return qtyentered;
	}
	public void setQtyentered(BigDecimal qtyentered) {
		this.qtyentered = qtyentered;
	}
	public BigDecimal getPriceentered() {
		return priceentered;
	}
	public void setPriceentered(BigDecimal priceentered) {
		this.priceentered = priceentered;
	}
	public String getLinedescription() {
		return linedescription;
	}
	public void setLinedescription(String linedescription) {
		this.linedescription = linedescription;
	}
	
	/*public String getTaxindicator() {
		return taxindicator;
	}
	public void setTaxindicator(String taxindicator) {
		this.taxindicator = taxindicator;
	}
	public BigDecimal getTaxamt() {
		return taxamt;
	}
	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}
	public int getC_tax_id() {
		return c_tax_id;
	}
	public void setC_tax_id(int c_tax_id) {
		this.c_tax_id = c_tax_id;
	}*/
	public String getBpvalue() {
		return bpvalue;
	}
	public void setBpvalue(String bpvalue) {
		this.bpvalue = bpvalue;
	}
	public String getBpname() {
		return bpname;
	}
	public void setBpname(String bpname) {
		this.bpname = bpname;
	}
	public String getAd_language() {
		return ad_language;
	}
	public void setAd_language(String ad_language) {
		this.ad_language = ad_language;
	}
	public Timestamp getDateinvoiced() {
		return dateinvoiced;
	}
	public void setDateinvoiced(Timestamp dateinvoiced) {
		this.dateinvoiced = dateinvoiced;
	}
	public String getOrderdocumentno() {
		return orderdocumentno;
	}
	public void setOrderdocumentno(String orderdocumentno) {
		this.orderdocumentno = orderdocumentno;
	}
	public String getOrderporeference() {
		return orderporeference;
	}
	public void setOrderporeference(String orderporeference) {
		this.orderporeference = orderporeference;
	}
	public String getBpaddress1() {
		return bpaddress1;
	}
	public void setBpaddress1(String bpaddress1) {
		this.bpaddress1 = bpaddress1;
	}
	public String getBpaddress2() {
		return bpaddress2;
	}
	public void setBpaddress2(String bpaddress2) {
		this.bpaddress2 = bpaddress2;
	}
	public String getBpaddress3() {
		return bpaddress3;
	}
	public void setBpaddress3(String bpaddress3) {
		this.bpaddress3 = bpaddress3;
	}
	public String getBpaddress4() {
		return bpaddress4;
	}
	public void setBpaddress4(String bpaddress4) {
		this.bpaddress4 = bpaddress4;
	}
	public String getBplocationname() {
		return bplocationname;
	}
	public void setBplocationname(String bplocationname) {
		this.bplocationname = bplocationname;
	}
	public String getBpcity() {
		return bpcity;
	}
	public void setBpcity(String bpcity) {
		this.bpcity = bpcity;
	}
	public String getBpregion() {
		return bpregion;
	}
	public void setBpregion(String cpregion) {
		this.bpregion = cpregion;
	}
	public String getPaymentterm() {
		return paymentterm;
	}
	public void setPaymentterm(String paymentterm) {
		this.paymentterm = paymentterm;
	}
	public String getSalesrepname() {
		return salesrepname;
	}
	public void setSalesrepname(String salesrepname) {
		this.salesrepname = salesrepname;
	}
	public BigDecimal getQtyinvoiced() {
		return qtyinvoiced;
	}
	public void setQtyinvoiced(BigDecimal qtyinvoiced) {
		this.qtyinvoiced = qtyinvoiced;
	}
	public BigDecimal getPricelist() {
		return pricelist;
	}
	public void setPricelist(BigDecimal pricelist) {
		this.pricelist = pricelist;
	}
	public BigDecimal getTaxrate() {
		return taxrate;
	}
	public void setTaxrate(BigDecimal taxrate) {
		this.taxrate = taxrate;
	}
	public String getDocBaseType() {
		return DocBaseType;
	}
	public void setDocBaseType(String docBaseType) {
		DocBaseType = docBaseType;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getUPC() {
		return UPC;
	}
	public void setUPC(String uPC) {
		UPC = uPC;
	}
	
	
}
